<?php
/**
 * @file
 * Contains main class for Conference Web Forms plugin for UW Restful
 * plugin.
 */

/**
 * Export conference web form content type.
 */
class RestfulConferenceWebFormsResource extends RestfulEntityBaseConferenceNode {
  /**
   * Overrides RestfulEntityBaseNode::publicFieldsInfo().
   */
  public function publicFieldsInfo() {
    $public_fields = parent::publicFieldsInfo();

    $public_fields['alias'] = array(
      'callback' => array($this, 'getEntityAlias'),
    );

    $public_fields['source'] = array(
      'callback' => array($this, 'getEntitySource'),
    );

    $public_fields['title'] = array(
      'property' => 'title',
    );

    $public_fields['body'] = array(
      'property' => 'body',
      'sub_property' => 'value',
      'process_callbacks' => array(
        array($this, 'contentSource')
      ),
    );

    $public_fields['summary'] = array(
      'property' => 'body',
      'sub_property' => 'summary',
    );

    $public_fields['rendered_html'] = array(
      'callback' => array($this, 'getRenderedHtml'),
    );

    $public_fields['featured_image'] = array(
      'property' => 'field_event_image',
      'process_callbacks' => array(
        array($this, 'imageProcess'),
      ),
    );

    $public_fields['metatags'] = array(
      'property' => 'nid',
      'process_callbacks' => array(
        array($this, 'getMetaTags'),
      ),
    );
    return $public_fields;
  }

  /**
   * Get rendered HTML for body with form embedded in it.
   *
   * @param \EntityDrupalWrapper $wrapper
   *  The wrapped entity.
   *
   * @return string
   *  The rendered HTML.
   */
  protected function getRenderedHtml(\EntityDrupalWrapper $wrapper) {
    $values = $wrapper->value();
    return render(node_view(node_load($values->nid), 'full', NULL));
  }
}
