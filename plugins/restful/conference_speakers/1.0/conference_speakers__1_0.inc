<?php
/**
 * @file
 * Contains Conference Speakers plugin for UW Restful Conference.
 */

$plugin = array(
  'label' => t('Conference Speakers'),
  'resource' => 'conference_speakers',
  'name' => 'conference_speakers__1_0',
  'entity_type' => 'node',
  'bundle' => 'conference_speaker',
  'description' => t('Export the conference speaker content type.'),
  'class' => 'RestfulConferenceSpeakersResource',
);
