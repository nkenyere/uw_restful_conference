<?php
/**
 * @file
 * Contains main class for Conference Speakers for UW Restful Conference.
 */

/**
 * Export conference speaker content type.
 */
class RestfulConferenceSpeakersResource extends RestfulEntityBaseConferenceNode{
  /**
   * Overrides RestfulEntityBaseNode::publicFieldsInfo().
   */
  public function publicFieldsInfo() {
    $public_fields = parent::publicFieldsInfo();

    $public_fields['alias'] = array(
      'callback' => array($this, 'getEntityAlias'),
    );

    $public_fields['source'] = array(
      'callback' => array($this, 'getEntitySource'),
    );

    $public_fields['title'] = array(
      'property' => 'title',
    );

    $public_fields['bio'] = array(
      'property' => 'body',
      'sub_property' => 'value',
      'process_callbacks' => array(
        array($this, 'contentSource')
      ),
    );

    $public_fields['summary'] = array(
      'property' => 'body',
      'sub_property' => 'summary',
    );

    $public_fields['first_name'] = array(
      'property' => 'field_first_name',
    );

    $public_fields['last_name'] = array(
      'property' => 'field_last_name',
    );

    $public_fields['position'] = array(
      'property' => 'field_profile_title',
    );

    $public_fields['affiliation'] = array(
      'property' => 'field_profile_affiliation',
    );

    $public_fields['twitter_handle'] = array(
      'property' => 'field_conference_twitter_handle',
    );

    $public_fields['promote'] = array(
      'property' => 'promote',
    );

    $public_fields['avatar'] = array(
      'property' => 'field_profile_photo',
      'process_callbacks' => array(
        array($this, 'imageProcess'),
      ),
    );

    $public_fields['speaker_group'] = array(
      'property' => 'field_conference_speaker_group',
      'sub_property' => 'tid',
      'process_callbacks' => array(
        array($this, 'getEntityTerm'),
      ),
    );

    return $public_fields;
  }
}
