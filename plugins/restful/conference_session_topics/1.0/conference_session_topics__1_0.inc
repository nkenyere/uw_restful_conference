<?php
/**
 * @file
 * Contains Conference Session Topics plugin for UW Restful Conference.
 */

$plugin = array(
  'label' => t('Conference Session Topics'),
  'resource' => 'conference_session_topics',
  'name' => 'conference_session_topics__1_0',
  'entity_type' => 'taxonomy_term',
  'bundle' => 'conference_session_topics',
  'description' => t('Export the conference session topics.'),
  'class' => 'RestfulConferenceSessionTopicsResource',
);
