<?php
/**
 * @file
 * Contains main class for Conference Sponsorship Levels plugin for UW
 * Restful Conference.
 */

/**
 * Export conference sponsorship levels taxonomy with sponsor content type
 * nested in each level.
 */
class RestfulConferenceSponsorshipLevelsResource extends RestfulEntityBaseConferenceTaxonomyTerm {
  /**
   * Overrides RestfulEntityBaseNode::publicFieldsInfo().
   */
  public function publicFieldsInfo() {
    $public_fields = parent::publicFieldsInfo();

    $public_fields['alias'] = array(
      'callback' => array($this, 'getEntityAlias'),
    );

    $public_fields['source'] = array(
      'callback' => array($this, 'getEntitySource'),
    );

    $public_fields['sponsors'] = array(
      'property' => 'tid',
      'process_callbacks' => array(
        array($this, 'getSponsors'),
      ),
    );

    $public_fields['description'] = array(
      'property' => 'description',
    );

    $public_fields['name'] = array(
      'property' => 'name',
    );

    $public_fields['metatags'] = array(
      'property' => 'tid',
      'process_callbacks' => array(
        array($this, 'getMetaTags'),
      ),
    );

    return $public_fields;
  }

  /**
   * Get entity's sponsors
   *
   * @param integer
   *    The entity id.
   *
   * @return array
   *  The sponsors for this sponsorship level.
   */
  protected function getSponsors($tid) {
    $return_array = array();

    $sponsors = taxonomy_select_nodes($tid, $pager = FALSE, $limit = FALSE);
    foreach ($sponsors as $sponsor) {
      $node = node_load($sponsor);
      $values = entity_metadata_wrapper('node', $node);
      $return_array[] = array(
        'id' => $node->nid,
        'label' => $node->title,
        'title' => $node->title,
        'logo' => $this->imageProcess($values->field_conference_sponsor_logo->value()),
        'link' => $values->field_conference_sponsor_url->value(),
      );
    }

    return $return_array;
  }

  /**
   * Process callback, Remove Drupal specific items from the image array.
   *
   * @param array $value
   *   The image array.
   *
   * @return array
   *   A cleaned image array.
   */
  protected function imageProcess($value) {
    return RestfulEntityBaseConferenceNode::processImage($value);
  }
}
