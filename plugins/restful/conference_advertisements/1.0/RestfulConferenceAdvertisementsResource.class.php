<?php
/**
 * @file
 * Contains main class for Conference Advertisements plugin for UW Restful
 * Conference.
 */

/**
 * Export the conference advertisement type.
 */
class RestfulConferenceAdvertisementsResource extends RestfulEntityBaseConferenceNode {
  /**
   * Overrides RestfulEntityBaseNode::publicFieldsInfo().
   */
  public function publicFieldsInfo() {
    $public_fields = parent::publicFieldsInfo();

    $public_fields['alias'] = array(
      'callback' => array($this, 'getEntityAlias'),
    );

    $public_fields['source'] = array(
      'callback' => array($this, 'getEntitySource'),
    );

    $public_fields['title'] = array(
      'property' => 'title',
    );

    $public_fields['sticky'] = array(
      'property' => 'sticky',
    );

    $public_fields['ad_image'] = array(
      'property' => 'field_conference_ad_image',
      'process_callbacks' => array(
        array($this, 'imageProcess'),
      ),
    );

    $public_fields['link_title'] = array(
      'property' => 'field_conference_ad_link',
      'sub_property' => 'title',
    );

    $public_fields['link_url'] = array(
      'property' => 'field_conference_ad_link',
      'sub_property' => 'url',
    );

    $public_fields['metatags'] = array(
      'property' => 'nid',
      'process_callbacks' => array(
        array($this, 'getMetaTags'),
      ),
    );

    return $public_fields;
  }
}
