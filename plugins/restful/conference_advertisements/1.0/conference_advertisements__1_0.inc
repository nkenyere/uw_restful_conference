<?php
/**
 * @file
 * Contains Conference Advertisements plugin for UW Restful Conference.
 */

$plugin = array(
  'label' => t('Conference Advertisements'),
  'resource' => 'conference_advertisements',
  'name' => 'conference_advertisements__1_0',
  'entity_type' => 'node',
  'bundle' => 'conference_advertisement',
  'description' => t('Export the conference advertisement content type.'),
  'class' => 'RestfulConferenceAdvertisementsResource',
);
