<?php
/**
 * @file
 * Contains main class for Conference Sessions plugin for UW Restful Conference.
 */

/**
 * Export conference session content type.
 */
class RestfulConferenceSessionsResource extends RestfulEntityBaseConferenceNode {
  /**
   * Overrides RestfulEntityBaseNode::publicFieldsInfo().
   */
  public function publicFieldsInfo() {
    $public_fields = parent::publicFieldsInfo();

    $public_fields['alias'] = array(
      'callback' => array($this, 'getEntityAlias'),
    );

    $public_fields['source'] = array(
      'callback' => array($this, 'getEntitySource'),
    );

    $public_fields['title'] = array(
      'property' => 'title',
    );

    $public_fields['summary'] = array(
      'property' => 'body',
      'sub_property' => 'value',
    );

    $public_fields['body'] = array(
      'property' => 'body',
      'sub_property' => 'summary',
      'process_callbacks' => array(
        array($this, 'contentSource')
      ),
    );

    $public_fields['date'] = array(
      'property' => 'field_conference_session_date'
    );

    $public_fields['speakers'] = array(
      'property' => 'field_conference_speakers',
      'sub_property' => 'nid',
      'process_callbacks' => array(
        array($this, 'getSpeakers'),
      ),
    );

    $public_fields['topics'] = array(
      'property' => 'field_conference_session_topics',
      'sub_property' => 'tid',
      'process_callbacks' => array(
        array($this, 'getEntityTerms'),
      ),
    );

    $public_fields['type'] = array(
      'property' => 'field_conference_session_type',
      'sub_property' => 'tid',
      'process_callbacks' => array(
        array($this, 'getEntityTerm'),
      ),
    );

    $public_fields['panelists'] = array(
      'property' => 'field_conference_panelists',
      'sub_property' => 'nid',
      'process_callbacks' => array(
        array($this, 'getSpeakers'),
      ),
    );

    $public_fields['moderator'] = array(
      'property' => 'field_conference_moderator',
      'sub_property' => 'nid',
      'process_callbacks' => array(
        array($this, 'getSpeaker'),
      ),
    );

    $public_fields['metatags'] = array(
      'property' => 'nid',
      'process_callbacks' => array(
        array($this, 'getMetaTags'),
      ),
    );

    return $public_fields;
  }

  /**
   * Load nodes then:
   * Delegates to RestfulEntityUtils::processSpeakers($nodes)
   */
  protected function getSpeakers($nids) {
    $nodes = array();
    foreach ($nids as $nid) {
      $nodes[] = node_load($nid);
    }
    return RestfulEntityUtils::processSpeakers($nodes);
  }

  /**
   * Load node then:
   * Delegates to RestfulEntityUtils::processSpeaker($node)
   */
  protected function getSpeaker($nid) {
    $node = node_load($nid);
    return RestfulEntityUtils::processSpeaker($node);
  }
}
