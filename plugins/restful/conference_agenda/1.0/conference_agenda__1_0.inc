<?php
/**
 * @file
 * Contains Conference Agenda plugin for UW Restful Conference.
 */

$plugin = array(
  'label' => t('Conference Agenda'),
  'resource' => 'conference_agenda',
  'name' => 'conference_agenda__1_0',
  'description' => t('Expose conference sessions to the REST API formatted as an agenda.'),
  'class' => 'RestfulConferenceAgendaResource',
  'render_cache' => array(
    'render' => TRUE,
  ),
);
