<?php
/**
 * @file
 * Contains the main class for Conference Agenda Plugin for UW Restful
 * Conference.
 */

/**
 * Exports session content type formatted as a multi-day agenda.
 */
class RestfulConferenceAgendaResource extends \RestfulDataProviderVariable {
  /**
   * Overrides RestfulDataProviderVariable::publicFieldsInfo().
   */
  public function publicFieldsInfo() {
    return array();
  }

  /**
   * Overrides RestfulDataProviderVariable::index()
   */
  public function index() {
    $query   = new EntityFieldQuery();
    $result  = $query->entityCondition('entity_type', 'node')
    ->entityCondition('bundle', 'conference_session')
    ->execute();
    if ($result && is_array($result) && isset($result['node'])) {
      $nodes = node_load_multiple(array_keys($result['node']));

      $nodes = $this->sortSessions($nodes);
      $sessions = $this->processSessions($nodes);

      return $this->buildAgenda($sessions);
    }

    return array();
  }

  /**
   * Check if a schedule already exists in the agenda
   *
   * @param string $date
   *  The date of the schedule
   *
   * @param array $agenda
   *  The agenda
   *
   * @return boolean
   *  Returns true if the schedule already exists in the agenda
   */
  protected function scheduleExists($date, $agenda) {
    $date_exists = FALSE;

    if (!$agenda || empty($agenda)) {
      return $date_exists;
    }

    foreach ($agenda as $schedule) {
      if ($schedule['date'] == $date) {
        $date_exists = TRUE;
        break;
      }
    }

    return $date_exists;
  }

  /**
   * Return the index of a schedule within an agenda.
   *
   * @param string $date
   *  The date of the schedule
   *
   * @param array $agenda
   *  The agenda
   *
   * @return int
   *  Returns the index of the schedule. Returns -1 for not found.
   */
  protected function getScheduleIndex($date, $agenda) {
    $index = -1;

    for ($i=0; $i<count($agenda); $i++) {
      if ($agenda[$i]['date'] == $date) {
        $index = $i;
      }
    }

    return $index;
  }

  /**
   * Return an array of sessions nodes sorted by the start date
   *
   * @param array $nodes
   *  An array of nodes (bundle: conference_session)
   *
   * @return array
   *  An array of sorted nodes
   */
  protected function sortSessions($nodes) {
    usort($nodes, function($a, $b) {
      $aValues = entity_metadata_wrapper('node', $a);
      $bValues = entity_metadata_wrapper('node', $b);

      if ($aValues->field_conference_session_date->value() == $bValues->field_conference_session_date->value()) {
        return 0;
      }
      return ($aValues->field_conference_session_date->value() < $bValues->field_conference_session_date->value()) ? -1 : 1;
    });

    return $nodes;
  }

  /**
   * Return session objects with specific properties of the nodes.
   *
   * @param array $nodes
   *  An array of nodes (bundle: conference_session)
   *
   * @return array
   *  An array of processed nodes
   */
  protected function processSessions($nodes) {
    $sessions = array();

    foreach ($nodes as $node) {
      $sessions[] = $this->processSession($node);
    }

    return $sessions;
  }

  /**
   * Return a session object with specific properties of the node.
   *
   * @param object $node
   *  The node (bundle: conference_session)
   *
   * @return object
   *  The processed node
   */
  protected function processSession($node) {
    $session = new stdClass();
    $values = entity_metadata_wrapper('node', $node);


    $session->id = $values->nid->value();
    $session->title = $values->title->value();
    $session->body = $this->contentSource($values->body->value->value());
    $session->summary = $values->body->summary->value();
    $session->moderator = $this->processSpeaker($values->field_conference_moderator->value());
    $session->panelists = $this->processSpeakers($values->field_conference_panelists->value());
    $session->speakers = $values->field_conference_speakers->value();
    $session->topics = $values->field_conference_session_topics->value();
    $session->type = $this->processTerm($values->field_conference_session_type->value());
    $session->date = $values->field_conference_session_date->value();
    if (isset($session->date['value'])) {
      $start_date = date("M j", strtotime($session->date['value']));
      $start_time = date("g:i a", strtotime($session->date['value']));
    }
    if (isset($session->date['value2'])) {
      $end_date = date("M j", strtotime($session->date['value2']));
      $end_time = date("g:i a", strtotime($session->date['value2']));
    }
    $session->start_date = $start_date ? $start_date : NULL;
    $session->start_time = $start_time ? $start_time : NULL;
    $session->end_date = $end_date ? $end_date : NULL;
    $session->end_time = $end_time ? $end_time : NULL;

    return $session;
  }

  /**
   * Builds an agenda from an order array of sessions.
   *
   * @param array $sessions
   *  The array of sessions
   *
   * @return array
   *  An agenda, which is an array of schedules, which is an array of sessions
   */
  protected function buildAgenda($sessions) {
    $id = 0;
    $agenda = array();
    $count = 0;

    foreach ($sessions as $session) {
      if ($this->scheduleExists($session->start_date, $agenda)) {
        $index = $this->getScheduleIndex($session->start_date, $agenda);
        $agenda[$index]['sessions'][] = $session;
      }
      else {
        $id = $id + 1;
        $agenda[] = array(
          'id' => $id,
          'date' => $session->start_date,
          'sessions' => array($session),
        );
      }
    }

    return $agenda;
  }

  /**
   * Delegates to RestfulEntityUtils::processSpeakers($nodes)
   */
  protected function processSpeakers($nodes) {
    if (!$nodes) {
      return NULL;
    }
    return RestfulEntityUtils::processSpeakers($nodes);
  }

  /**
   * Delegates to RestfulEntityUtils::processSpeaker($node)
   */
  protected function processSpeaker($node) {
    if (!$node) {
      return NULL;
    }
    return RestfulEntityUtils::processSpeaker($node);
  }

  /**
   * Delegates to RestfulEntityUtils::processTerms($tersm)
   */
  protected function processTerms($terms) {
    if (!$terms) {
      return NULL;
    }
    return RestfulEntityUtils::processTerms($terms);
  }

  /**
   * Delegates to RestfulEntityUtils::processTerm($term)
   */
  protected function processTerm($term) {
    if (!$term) {
      return NULL;
    }
    return RestfulEntityUtils::processTerm($term);
  }

  /**
   * Delegates to RestfulEntityBaseConferenceNode::processImage($value)
   */
  protected function imageProcess($value) {
    if (!$value) {
      return NULL;
    }
    return RestfulEntityBaseConferenceNode::processImage($value);
  }

  /**
   * Process callback for formatting the content for formatting the content.
   *
   * @param string $value
   *   The current value.
   *
   * @return The new value.
   */
  protected function contentSource($value) {
    $filterValue = ckeditor_socialmedia_filter_process($value);
    $filterValue = uw_video_embed_filter_process($filterValue);
    if (module_exists('uw_ct_embedded_facts_and_figures') && function_exists('cke_ff_process')) {
      $filterValue = cke_ff_process($filterValue);
    }

    return $filterValue;
  }
}
