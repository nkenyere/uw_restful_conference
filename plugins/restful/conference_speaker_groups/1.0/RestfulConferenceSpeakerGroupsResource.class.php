<?php
/**
 * @file
 * Contains main class for Conference Speaker Groups plugin for UW Restful
 * Conference.
 */

/**
 * Export conference speaker groups taxonomy.
 */
class RestfulConferenceSpeakerGroupsResource extends RestfulEntityBaseConferenceTaxonomyTerm {
  /**
   * Overrides RestfulEntityBaseNode::publicFieldsInfo().
   */
  public function publicFieldsInfo() {
    $public_fields = parent::publicFieldsInfo();

    $public_fields['alias'] = array(
      'callback' => array($this, 'getEntityAlias'),
    );

    $public_fields['source'] = array(
      'callback' => array($this, 'getEntitySource'),
    );

    $public_fields['speakers'] = array(
      'property' => 'tid',
      'process_callbacks' => array(
        array($this, 'getSpeakers'),
      ),
    );

    $public_fields['description'] = array(
      'property' => 'description',
    );

    $public_fields['name'] = array(
      'property' => 'name',
    );

    $public_fields['metatags'] = array(
      'property' => 'tid',
      'process_callbacks' => array(
        array($this, 'getMetaTags'),
      ),
    );

    return $public_fields;
  }

  /**
   * Get entity's speakers
   *
   * @param integer
   *  The entity id.
   *
   * @return array
   *  The metatag information.
   */
  protected function getSpeakers($tid) {
    $speakers = array();

    $nids = taxonomy_select_nodes($tid, $pager = FALSE, $limit = FALSE);
    foreach ($nids as $nid) {
      $node   = node_load($nid);
      $speakers[] = RestfulEntityUtils::processSpeaker($node);
    }

    return $speakers;
  }

  /**
   * Delegate to RestfulEntityBaseConferenceNode::processImage($value)
   */
  protected function imageProcess($value) {
    return RestfulEntityBaseConferenceNode::processImage($value);
  }
}
