<?php
/**
 * @file
 * Contains Conference Sponsors plugin for UW Restful Conference.
 */

$plugin = array(
  'label' => t('Conference Sponsors'),
  'resource' => 'conference_sponsors',
  'name' => 'conference_sponsors__1_0',
  'entity_type' => 'node',
  'bundle' => 'conference_sponsor',
  'description' => t('Export the conference sponsor content type.'),
  'class' => 'RestfulConferenceSponsorsResource',
);
