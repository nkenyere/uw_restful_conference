<?php
/**
 * @file
 * Contains main class for Conference Sponsors plugin for UW Restful
 * Conference.
 */

/**
 * Export conference sponsor content type.
 */
class RestfulConferenceSponsorsResource extends RestfulEntityBaseConferenceNode {
  /**
   * Overrides RestfulEntityBaseNode::publicFieldsInfo().
   */
  public function publicFieldsInfo() {
    $public_fields = parent::publicFieldsInfo();

    $public_fields['alias'] = array(
      'callback' => array($this, 'getEntityAlias'),
    );

    $public_fields['source'] = array(
      'callback' => array($this, 'getEntitySource'),
    );

    $public_fields['title'] = array(
      'property' => 'title',
    );

    $public_fields['logo'] = array(
      'property' => 'field_conference_sponsor_logo',
      'process_callbacks' => array(
        array($this, 'imageProcess'),
      ),
      'image_styles' => array('original', 'thumbnail', 'medium', 'large', 'size540x370', 'cover_photo', 'cover_photo_x2', 'conference_sponsor', 'conference_speaker', 'conference_masthead'),
    );

    $public_fields['link'] = array(
      'property' => 'field_conference_sponsor_url'
    );

    $public_fields['sponsorship_level'] = array(
      'property' => 'field_conference_sponsor_level',
      'sub_property' => 'tid',
      'process_callbacks' => array(
        array($this, 'getEntityTerm'),
      ),
    );

    return $public_fields;
  }
}
