<?php
/**
 * @file
 * Contains main class for Conference Links plugin for UW Restful Conference.
 */

/**
 * Export the conference links type.
 */
class RestfulConferenceLinksResource extends RestfulEntityBaseConferenceNode {
  /**
   * Overrides RestfulEntityBaseNode::publicFieldsInfo().
   */
  public function publicFieldsInfo() {
    $public_fields = parent::publicFieldsInfo();

    $public_fields['alias'] = array(
      'callback' => array($this, 'getEntityAlias'),
    );

    $public_fields['source'] = array(
      'callback' => array($this, 'getEntitySource'),
    );

    $public_fields['title'] = array(
      'property' => 'title',
    );

    $public_fields['links'] = array(
      'property' => 'field_conference_links',
    );

    $public_fields['metatags'] = array(
      'property' => 'nid',
      'process_callbacks' => array(
        array($this, 'getMetaTags'),
      ),
    );

    return $public_fields;
  }
}
