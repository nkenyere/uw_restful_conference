<?php
/**
 * @file
 * Contains main class for Conference Tweets plugin for UW Restful Conference.
 */

require_once('TwitterAPIExchange.php');

/**
 * Export list of tweets.
 */
class RestfulConferenceTweetsResource extends \RestfulDataProviderVariable {
  /**
   * {@inheritdoc}
   */
  public function publicFieldsInfo() {
    return array();
  }

  /**
   * Get tweets
   */
  public function index() {
    $global_settings;
    // Map name and value to an indexed array structure.
    foreach ($GLOBALS['conf'] as $variable_name => $variable_value) {
      $global_settings[] = array(
        'name' => $variable_name,
        'value' => $variable_value,
      );
    }

    $settings = array(
      'oauth_access_token' => $this->getSetting($global_settings, 'conference_twitter_oauth_access_token'),
      'oauth_access_token_secret' => $this->getSetting($global_settings, 'conference_twitter_oauth_access_token_secret'),
      'consumer_key' => $this->getSetting($global_settings, 'conference_twitter_consumer_key'),
      'consumer_secret' => $this->getSetting($global_settings, 'conference_twitter_consumer_secret'),
    );

    $query = $this->getSetting($global_settings, 'conference_twitter_query_string');

    $url = 'https://api.twitter.com/1.1/search/tweets.json';
    $q = '?q=' . $query;
    $method = 'GET';

    $twitter = new TwitterAPIExchange($settings);
    $results = $twitter->setGetfield($q)->buildOauth($url, $method)->performRequest();

    $results = json_decode($results);

    if ($results && $results->statuses) {
      return $results->statuses;
    }

    return array();
  }

  /**
   * Get a setting.
   *
   * @param array $settings
   *  The settings.
   *
   * @param string $name
   *  The name/key of the setting to get.
   *
   * @return string
   *  The value of the setting.
   */
  protected function getSetting($settings, $name) {
    foreach ($settings as $setting) {
      if ($setting['name'] == $name) {
        return $setting['value'];
      }
    }

    return NULL;
  }
}
