<?php
/**
 * @file
 * Contains Conference Videos plugin for UW Restful Conference.
 */
$plugin = array(
  'label' => t('Conference Videos'),
  'resource' => 'conference_videos',
  'name' => 'conference_videos__1_0',
  'entity_type' => 'node',
  'bundle' => 'conference_video',
  'description' => t('Export the conference video content type.'),
  'class' => 'RestfulConferenceVideosResource',
);
