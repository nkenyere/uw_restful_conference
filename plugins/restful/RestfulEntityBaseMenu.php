<?php
/**
 * @file
 * Contains RestfulEntityBaseMenu.
 */

/**
 * A base implementation for "Menu" entity type.
 */
class RestfulEntityBaseMenu extends RestfulEntityBase {
  /**
   * Overrides \RestfulEntityBase::defaultSortInfo().
   */
   public function defaultSortInfo() {
     // Sort by 'weight' property in ascending order.
     return array('weight' => 'ASC');
   }

  /**
   * Overrides \RestfulEntityBase::publicFieldsInfo().
   */
  public function publicFieldsInfo() {
    $public_fields = parent::publicFieldsInfo();

    $public_fields['weight'] = array(
      'property' => 'weight',
    );

    $public_fields['path'] = array(
      'property' => 'link_path',
    );

    $public_fields['alias'] = array(
      'callback' => array($this, 'getEntityAlias'),
    );

    $public_fields['nid'] = array(
      'callback' => array($this, 'getNid'),
    );

    return $public_fields;
  }

  /**
   * Get entity's alias.
   *
   * @param \EntityDrupalWrapper $wrapper
   *  The wrapped entity.
   *
   * @return string
   *  The alias URL.
   */
  protected function getEntityAlias(\EntityDrupalWrapper $wrapper) {
    $values = $wrapper->value();

    if ($values->link_path == '<front>') {
      return '';
    }
    else {
      return drupal_get_path_alias($values->link_path);
    }
  }

  /**
   * Get entity's node id.
   *
   * @param \EntityDrupalWrapper $wrapper
   *  The wrapped entity
   *
   * @return string
   *  The alias URL.
   */
  protected function getNid(\EntityDrupalWrapper $wrapper) {
    $values = $wrapper->value();
    return $this->getNodeFromAlias($values->link_path);
  }

  /**
   * Get a node from an alias.
   *
   * @param string $alias
   *  The entity's node alias.
   *
   * @return string
   *  The node.
   */
  protected function getNodeFromAlias($alias) {
    if ($alias == '<front>') {
      $path  = drupal_get_normal_path(variable_get('site_frontpage', 'node/1'));
    }
    else {
      $path = drupal_get_normal_path($alias);
    }

    if ($path) {
      $node = menu_get_object("node", 1, $path);
      if (!$node) {
        $path = drupal_lookup_path("source", $path);
        if ($path) {
          $node = menu_get_object("node", 1, $path);
          if ($node) {
            return $node->nid;
          }
        }
      }

      if ($node) {
        return $node->nid;
      }
    }

    return NULL;
  }

  /**
   * Overrides RestfulEntityBase::getQueryForList().
   *
   * Exposes only non-hidden/enabled links.
   */
  public function getQueryForList() {
    $query = parent::getQueryForList();
    $query->propertyCondition('hidden', 0);
    return $query;
  }
}
